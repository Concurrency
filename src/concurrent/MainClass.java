package concurrent;

import java.util.concurrent.Semaphore;

public class MainClass {

	private static int n = 10;
	private static int count = 0;
	private static Semaphore mutex = new Semaphore(1);
	private static Semaphore barrier = new Semaphore(0);
	private static Thread[] threads = new Thread[n];

	public static void main(String[] args) {

		for (int i = 0; i < n; i++) {
			threads[i] = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						mutex.acquire();
						System.out.println("Mutex acquired");
						count++;
						System.out.println("Mutex released");
						mutex.release();

						if (count == n) {
							System.out.println("Barrier released");
							barrier.release();
						}

						System.out.println("Barrier acquired");
						barrier.acquire();

						System.out.println("Barrier released");
						barrier.release();

						System.out
								.println("This line should be executed by all threads at the same time");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}

			});

			threads[i].start();
		}
	}

	public void showFive() {
		System.out.println("Five");
	}

	public void showTen() {
		System.out.println("Ten");
	}
}
